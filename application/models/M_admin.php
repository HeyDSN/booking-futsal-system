<?php

Class M_admin extends CI_Model {

    function getInvoiceAll(){
        return $this->db->query("SELECT invoice.id_inv, harga, dp, `status`, stamp, email, hp, `user`.nama as nama, (SELECT COUNT(*) FROM book WHERE book.id_inv = invoice.id_inv) as jlapangan, bank, payment.nama as pengirim, foto_bukti FROM invoice INNER JOIN `user` ON invoice.id_user = `user`.id_user LEFT JOIN payment ON invoice.id_inv = payment.id_inv")->result();
    }

    function getJSONBooking(){
        return $this->db->query("SELECT * FROM book")->result();
    }

}
