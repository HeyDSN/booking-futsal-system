<?php

Class M_user extends CI_Model {

    function login($data){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $data['email']);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 1) {
            $hash = $query->row()->password;
            if(password_verify($data['password'], $hash)){
                unset($data['password']);
                return $query->result();
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    function getInvoiceAll(){
        $idUsr = $this->session->userdata('ID');
        return $this->db->query("SELECT *, (SELECT COUNT(*) FROM book WHERE book.id_inv = invoice.id_inv) as jlapangan FROM invoice WHERE id_user = $idUsr")->result();
    }
}
