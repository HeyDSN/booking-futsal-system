 <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVIGASI</li>
        <?php if($this->session->userdata('LEVEL') != '99'){ ?>
        <li>
          <a href="<?php echo base_url('admin/panel.html'); ?>">
          <i class="fa fa-dashboard"></i><span>Beranda</span></a>
        </li>
        <?php } ?>
        <li>
          <a href="<?php echo base_url('admin/user.html'); ?>">
          <i class="fa fa-users"></i><span>Manajemen Pelanggan</span></a>
        </li>
        <?php if($this->session->userdata('LEVEL') != '99'){ ?>
        <li>
          <a href="<?php echo base_url('admin/event.html'); ?>">
          <i class="fa fa-tags"></i><span>Kelola Event</span></a>
        </li>
        <?php } ?>
        <?php if($this->session->userdata('LEVEL') == '99'){ ?>
        <li>
          <a href="<?php echo base_url('admin/laporan.html'); ?>">
          <i class="fa fa-file"></i><span>Laporan Bulanan</span></a>
        </li>
        <?php } ?>
        <li>
          <a href="<?php echo base_url('admin/config.html'); ?>">
          <i class="fa fa-gear"></i><span>Kelola Website</span></a>
        </li>
        </li>
      </ul>
    </section>
    
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->
