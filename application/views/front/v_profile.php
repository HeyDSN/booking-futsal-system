 <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>Profile</h1>
      </section>

      <!-- Main content -->
      <section class="content">
      <div class="row">
        <div class="col-xs-3">
             <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center"><?php echo $this->session->userdata('NAMA'); ?></h3>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Tanggal Bergabung</b> <a class="pull-right"><?php echo date('d/m/Y', strtotime($user->createdate)); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Total Booking</b> <a class="pull-right"><?php echo $jmlBook->jml; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Lapangan Favorit</b> <a class="pull-right"><?php if($favLap==null){ echo '-'; }else{ echo 'Lapangan '.$favLap->lapangan;} ?></a>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-xs-9">
          <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Edit Profile</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
               <form class="form-horizontal" action="" method="post">
                  <div class="form-group">
                    <label  class="col-sm-2 control-label">Email</label>
                    <p class="col-sm-10 control-label" style="text-align: left; !important"><?php echo $this->session->userdata('EMAIL'); ?></p>
                  </div>
                  <div class="form-group">
                    <label  class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-10">
                      <input name="nama" type="text" class="form-control" required value="<?php echo $user->nama; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label  class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                      <textarea name="alamat" id="alamat" class="form-control" required><?php echo $user->alamat; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label  class="col-sm-2 control-label">No. HP</label>
                    <div class="col-sm-10">
                      <input name="hp" type="number" class="form-control" required value="<?php echo $user->hp; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" name="chProfile" value="true" class="btn btn-success">Simpan</button>
                    </div>
                  </div>
                </form>
                <hr/>
                <form class="form-horizontal" action="" method="post">
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Sandi Saat Ini</label>
                    <div class="col-sm-10">
                      <input type="password" name="opassword" class="form-control" required placeholder="Sandi Saat Ini">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Sandi Baru</label>
                    <div class="col-sm-10">
                      <input id="newPass" type="password" name="password" class="form-control" required placeholder="Sandi Baru">
                    </div>
                  </div>
                  <div id="password" class="form-group">
                    <label class="col-sm-2 control-label">Ulangi Sandi Baru</label>
                    <div class="col-sm-10">
                      <input id="confPass" type="password" class="form-control" required placeholder="Ulangi Sandi Baru" onChange="checkPasswordMatch();">
                      <span class="help-block" id="notice"></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button id="pc" type="submit" name="chPw" value="true" disabled="true" class="btn btn-success">Simpan</button>
                    </div>
                  </div>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->