 <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php $status = $inv->status;  if($status=="Booking Selesai"){echo "Kwitansi Anda";}else{echo "Invoice Anda";}; ?>
        </h1>
      </section>
      <?php if(!isset($inv->stamp)){
        redirect('user/panel');
      } ?>
      <!-- Main content -->
      <section class="content">
      <div class="row">
         <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-soccer-ball-o"></i> AWK Futsal
            <small class="pull-right">Date: <?php echo date('d/m/Y', strtotime($inv->stamp)); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>AWK Futsal</strong><br>
            Jatiwaringin Bekasi<br>
            Pondok Gede<br>
            Telepon: 82268456<br>
            Email: AWK Futsal@gmail.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?php echo $user->nama; ?></strong><br>
            Email: <?php echo $user->email; ?> <br>    
            Indonesia
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b><?php if($status=="Booking Selesai"){echo "Kwitansi";}else{echo "Invoice";}; ?> #<?php echo $inv->id_inv; ?></b><br>
          <?php
          $status = $inv->status;
          if($inv->status == "Konfirmasi Berhasil" || $status == "Booking Selesai"){
            $sisa = $inv->harga - $inv->dp;
            if($sisa == 0){
              $pesan = "Lunas";
              $color = "bg-green";
            }else{
              $pesan = "Lunas DP";
              $color = "bg-green";
            }
          }elseif($inv->status == "Booking Dibatalkan"){
            $pesan = "Pesanan Dibatalkan";
            $color = "bg-gray";
          }else{
            if($inv->status == "Proses Konfirmasi"){
              $pesan = "Proses Konfirmasi";
              $color = "bg-orange";
            }else{
              $pesan = "Belum Lunas";
              $color = "bg-red";
            }
          }
          ?>
          <b>Status :</b> <small class="label <?php echo $color ?>"><?php echo $pesan; ?></small><br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Deskripsi</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($book as $row){ ?>
              <tr>
                <td><?php
                $json = json_decode($row->book_time);
                $first = $json[0];
                $last = end($json);
                echo "$row->book_date / Lapangan $row->lapangan / $first - $last / $row->hour_length Jam</td>";?>
                <td>
                  <?php
                  $subtotal = $harga->value * $row->hour_length;
                  echo rp($subtotal);
                  ?>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Mohon lakukan pembayaran DP ke <br><br>
            Bank : <b><?php echo $bank->value; ?></b><br>
            No Rekening : <?php echo $rekening->value; ?><br>
            Atas Nama   : <?php echo $nama_rek->value; ?>
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead"></p>

          <div class="table-responsive">
            <table class="table">
              <!-- <tr>
                <th style="width:50%">Subtotal:</th>
                <td>Rp. 100,000</td>
              </tr> -->
              <tr>
                <th style="text-align:right">Total Biaya:</th>
                <td style="width:45%"><?php echo rp($inv->harga); ?></td>
              </tr>
              <tr>
                <th style="text-align:right"><?php if($status=="Booking Selesai"){echo "Total Bayar:";}else{echo "Total Bayar/DP:";}; ?></th>
                <td style="width:45%"><?php echo rp($inv->dp); ?></td>
              </tr>
              <tr>
                <th style="text-align:right">Sisa Harus Dibayar:</th>
                <th style="width:45%"><?php $sisa = $inv->harga - $inv->dp; echo rp($sisa); ?></th>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <button type="button" class="btn btn-default pull-right" onclick="cetak()"><i class="fa fa-print"></i> Print
          </button>
        </div>
      </div>
    </section>
      </div>
      <!-- /.row -->
    </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->