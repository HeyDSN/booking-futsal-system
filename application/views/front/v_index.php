 <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <br>
        <h1>
          Selamat datang kembali, <?php echo $this->session->userdata('NAMA'); ?>
        </h1><br>
      </section>

      <!-- Main content -->
      <section class="content">
      <div class="row">
        <div class="col-xs-3">
          <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Peraturan AWK Futsal</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <?php echo $peraturan->value; ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-xs-9">
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Status Booking</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <table id="example2" class="table table-bordered table-striped dt-responsive">
                <thead>
                <tr>
                  <th>Invoice</th>
                  <th>Tanggal</th>
                  <th>Jumlah Lapangan</th>
                  <th>Biaya Total</th>
                  <th>Biaya DP</th>
                  <th>Status Booking</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($pesanan as $row){ ?>
                <tr>
                  <td style="vertical-align: middle;"><a href="<?php echo base_url('user/invoice/').$row->id_inv; ?>" target="_blank"><?php echo '#'.$row->id_inv; ?></a></td>
                  <td style="vertical-align: middle;"><?php echo date('d/m/Y', strtotime($row->stamp)); ?></td>
                  <td style="vertical-align: middle;"><?php echo $row->jlapangan." Lapangan"; ?></td>
                  <td style="vertical-align: middle;"><?php echo rp($row->harga); ?></td>
                  <td style="vertical-align: middle;"><?php
                  $dp = ($row->harga * 20) / 100;
                  echo rp($dp);
                  ?></td>
                  <td style="vertical-align: middle;">
                    <?php 
                    $status = $row->status;
                    if($status == "Menunggu Konfirmasi"){
                      $color = "bg-yellow";
                      $disabledk = false;
                      $disabledb = false;
                    }elseif($status == "Proses Konfirmasi"){
                      $color = "bg-blue";
                      $disabledk = true;
                      $disabledb = true;
                    }elseif($status == "Konfirmasi Berhasil"){
                      $color = "bg-green";
                      $disabledk = true;
                      $disabledb = true;
                    }elseif($status == "Konfirmasi Gagal"){
                      $color = "bg-red";
                      $disabledk = true;
                      $disabledb = true;
                    }elseif($status == "Booking Dibatalkan"){
                      $color = "bg-black";
                      $disabledk = true;
                      $disabledb = true;
                    }
                    elseif($status == "Booking Selesai"){
                      $color = "bg-teal";
                      $disabledk = true;
                      $disabledb = true;
                    }
                    ?>
                    <small class="label <?php echo $color; ?>" style="display:inline-block;width:100%"><?php echo $status; ?></small>
                  </td>
                  <td style="vertical-align: middle;">
                    <form action="<?php echo base_url('user/konfirmasi.html'); ?>" method="post">
                    <input type="hidden" name="idv" value="<?php echo $row->id_inv; ?>">
                    <input type="hidden" name="nml" value="<?php echo $dp; ?>">
                    <button type="submit" name="confirm" value="confirm" class="btn btn-sm btn-block btn-primary" <?php if($disabledk==true){echo 'disabled';} ?>><i class="fa fa-check"></i> Konfirmasi</button>
                    </form>
                    <div style="padding-top:10px"></div>
                    <form action="" method="post" onsubmit="return confirm('Yakin batalkan booking?');">
                    <input type="hidden" name="idv" value="<?php echo $row->id_inv; ?>">
                    <button type="submit" name="cancelBook" value="cancelBook" class="btn btn-sm btn-block btn-danger" <?php if($disabledb==true){echo 'disabled';} ?>><i class="fa fa-times"></i> Batalkan</button>
                    <div style="padding-top:10px"></div>
                    <a href="<?php echo base_url('user/invoice/').$row->id_inv; ?>" target="_blank" class="btn btn-sm btn-block btn-success"><i class="fa fa-eye"></i> Lihat Invoice</a>
                    </form>
                  </td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->