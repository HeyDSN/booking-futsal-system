<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('M_user');
        $this->load->model('M_global');
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function index(){
        redirect('auth/login');
    }

    public function register(){
		 if($this->session->userdata('LOGIN')==TRUE){
			if($this->session->userdata('LEVEL')==1){
				redirect('user/panel');
			}else{
				redirect('admin/panel');
			}
		}
		$this->load->view('front/v_register');
	}

	public function login(){
		if($this->session->userdata('LOGIN')==TRUE){
			if($this->session->userdata('LEVEL')==1){
				redirect('user/panel');
			}else{
				redirect('admin/panel');
			}
		}
		$this->load->view('v_login');
	}

    public function doregister(){
		if($this->input->post('register')){
			$options = [
				'cost' => 11
			];
			$hash = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);
			$data = array(
                'nama' => $this->input->post('nama'),
                'alamat' => $this->input->post('alamat'),
                'hp' => $this->input->post('hp'),
				'email' => $this->input->post('email'),
				'password' => $hash
            );
            $where = array(
				'email' => $this->input->post('email')
            );
            $check = $this->M_global->check('user',$where);
            if($check==true){
                $save = $this->M_global->insert('user',$data);
			    $this->session->set_flashdata('type','alert-success');
			    $this->session->set_flashdata('notif','Pendaftaran berhasil, silahkan login');
			    redirect('auth/login');
            }else{
                $this->session->set_flashdata('type','alert-danger');
                $this->session->set_flashdata('notif','Pendaftaran gagal, email telah terdaftar');
			    redirect('auth/login');
            }
			
		}else{
			redirect('auth/login');
		}
    }
    
    public function dologin(){
        if($this->input->post('login')){

            if($this->session->userdata('LOGIN')==TRUE){
                if($this->session->userdata('LEVEL')==1){
                    redirect('user/panel');
                }else{
                    redirect('admin/panel');
                }
            }

            $data = array(
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password')
            );
                    
            $result = $this->M_user->login($data);

            if($result){

                $sess_array = array();
                foreach($result as $row) {
                    $sess_array = array(
                        'ID'        => $row->id_user,
                        'EMAIL'      => $row->email,
                        'NAMA'      => $row->nama,
                        'LEVEL'      => $row->level,
                        'LOGIN'     => TRUE
                    );
                }

                $this->session->set_userdata($sess_array);
                if($this->session->userdata('LEVEL') == 1){
                    redirect('user/panel');
                }else{
                    redirect('admin/panel');
                }

            }else{

                $this->session->set_flashdata('type','alert-danger');
                $this->session->set_flashdata('notif','Login Gagal!! Username atau Password Salah !');
                redirect('auth/login');
                
            }

        }else{
			redirect('auth/login');
		}
    }

    public function dologout(){
        $sess_array = array(
            'ID'    => '',
            'EMAIL' => '',
            'NAMA'  => '',
            'LEVEL' => '',
            'LOGIN' => FALSE
        );

        $this->session->unset_userdata($sess_array);
        session_destroy();
        redirect('auth/login');
    }

}