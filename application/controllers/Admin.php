<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
        parent::__construct();
		$this->load->model('M_admin');
		$this->load->model('M_global');
		$halaman = $this->uri->segment(2);
		if ( !in_array($halaman, array('login','register'), true ) ) {
            if($this->session->userdata('LOGIN')!=TRUE){
				$this->session->set_flashdata('type','alert-warning');
                $this->session->set_flashdata('notif','Harap login terlebih dahulu sebelum memesan.');
                redirect('auth/login');
			}
			if($this->session->userdata('LEVEL')==1){
				redirect('user/panel');
			}
		}
	}

	public function index(){
		redirect('admin/panel');
	}
	public function panel(){
		if($this->session->userdata('LEVEL') == '99'){
			redirect('admin/laporan');
		}
		if($this->input->post('cancelBook')){
			$idv = $this->input->post('idv');
			$dataI = array(
				'status' => 'Konfirmasi Gagal'
			);
			$dataB = array(
				'status' => 0
			);
			$where = array(
				'id_inv' => $idv
			);
			$delInv = $this->M_global->update('invoice',$dataI,$where);
			$delBook = $this->M_global->update('book',$dataB,$where);
			echo "<script>alert('Booking telah dibatalkan.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		if($this->input->post('confirmBook')){
			$idv = $this->input->post('idv');
			$str = $this->input->post('jumlah');
			$str = substr($str, 0, -3);
			$jumlah = preg_replace('/[^0-9]/', '', $str);
			$status = "Konfirmasi Berhasil";

			$dataI = array(
				'dp' => $jumlah,
				'status' => $status
			);

			$where = array(
				'id_inv' => $idv
			);

			$updateInvoice = $this->M_global->update('invoice',$dataI,$where);
			echo "<script>alert('Booking berhasil dikonfirmasi.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		if($this->input->post('selesaiBook')){
			$idv = $this->input->post('idv');
			$jumlah = $this->input->post('jml');
			$status = "Booking Selesai";

			$dataI = array(
				'dp' => $jumlah,
				'status' => $status
			);

			$where = array(
				'id_inv' => $idv
			);

			$updateInvoice = $this->M_global->update('invoice',$dataI,$where);
			echo "<script>alert('Booking berhasil dikonfirmasi.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		$data['pesanan'] = $this->M_admin->getInvoiceAll();
		$data['json'] = $this->M_admin->getJSONBooking();
		$data['peraturan'] = $this->M_global->confGet('peraturan');
		$this->load->view('skeleton/skeleton_header');
		$this->load->view('skeleton/skeleton_navbar');
		$this->load->view('admin/v_home',$data);
		$this->load->view('skeleton/skeleton_footer');
	}
	public function user(){

		if($this->input->post('delUser')){
			$where = array(
				'id_user' => $this->input->post('idu')
			);
			$data['del'] = $this->M_global->del("user",$where);
			echo "<script>alert('Data pelanggan berhasil dihapus.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		$data['user'] = $this->M_global->custom("SELECT *, (SELECT COUNT(*)FROM invoice WHERE invoice.id_user = `user`.id_user) as jmlBook FROM `user` LEFT OUTER JOIN (SELECT id_user as idu, lapangan, COUNT(lapangan) as total FROM book GROUP BY id_user, lapangan ORDER BY total DESC LIMIT 1) as fav ON fav.idu = `user`.id_user WHERE `user`.`level` = 1");
		$this->load->view('skeleton/skeleton_header');
		$this->load->view('skeleton/skeleton_navbar');
		$this->load->view('admin/v_user',$data);
		$this->load->view('skeleton/skeleton_footer');
	}
	public function event(){

		if($this->input->post('addEv')){
			$judul = $this->input->post('judul');
			$isi = $this->input->post('isi');
			$add['event'] = $this->M_global->Incustom("INSERT INTO `event` (judul,isi) VALUES ('$judul','$isi')");
			echo "<script>alert('Event berhasil ditambahkan.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		if($this->input->post('delEv')){
			$where = array('id_event'=>$this->input->post('ide'));
			$add['event'] = $this->M_global->del('event',$where);
			echo "<script>alert('Event berhasil dihapus.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		$data['event'] = $this->M_global->custom("SELECT * FROM `event` ORDER BY tanggal DESC");

		$this->load->view('skeleton/skeleton_header');
		$this->load->view('skeleton/skeleton_navbar');
		$this->load->view('admin/v_manageevent',$data);
		$this->load->view('skeleton/skeleton_footer');
	}
	public function edit_event(){
		if($this->session->userdata('LEVEL') == '99'){
			redirect('admin/laporan');
		}
		if($this->input->post('upEv')){
			$id_event = $this->input->post('ide');
			$judul = $this->input->post('judul');
			$isi = $this->input->post('isi');
			$where=array('id_event' => $id_event);
			$data=array('judul' => $judul, 'isi' => $isi);
			
			$add['event'] = $this->M_global->update('event',$data,$where);
			echo "<script>alert('Event berhasil diperbarui.')</script>";
			echo "<script> window.location.href = 'event.html'</script>";
		}

		$id_event = $this->input->post('ide');
		$where=array('id_event' => $id_event);
		$data['event'] = $this->M_global->getOne('event',$where);

		$this->load->view('skeleton/skeleton_header');
		$this->load->view('skeleton/skeleton_navbar');
		$this->load->view('admin/v_editevent',$data);
		$this->load->view('skeleton/skeleton_footer');
	}
	public function config(){

		if($this->input->post('chMain')){
			$str = $this->input->post('harga');
			$str = substr($str, 0, -3);
			$harga = preg_replace('/[^0-9]/', '', $str);

			$dataH = array('value'=>$harga);
			$dataK = array('value'=>$this->input->post('kontak'));
			$dataA = array('value'=>$this->input->post('alamat'));
			$whereH = array('name'=>'harga');
			$whereK = array('name'=>'kontak');
			$whereA = array('name'=>'alamat');
			$update['harga'] = $this->M_global->update('kel_web', $dataH, $whereH);
			$update['kontak'] = $this->M_global->update('kel_web', $dataK, $whereK);
			$update['alamat'] = $this->M_global->update('kel_web', $dataA, $whereA);
			echo "<script>alert('Data berhasil diperbarui.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		if($this->input->post('chBank')){
			$dataB = array('value'=>$this->input->post('bank'));
			$dataR = array('value'=>$this->input->post('rekening'));
			$dataN = array('value'=>$this->input->post('namaRek'));
			$whereB = array('name'=>'bank');
			$whereR = array('name'=>'rekening');
			$whereN = array('name'=>'nama_rek');
			$update['bank'] = $this->M_global->update('kel_web', $dataB, $whereB);
			$update['rekening'] = $this->M_global->update('kel_web', $dataR, $whereR);
			$update['nama_rek'] = $this->M_global->update('kel_web', $dataN, $whereN);
			echo "<script>alert('Data berhasil diperbarui.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		if($this->input->post('chTentang')){
			$dataT = array('value'=>$this->input->post('tentang'));
			$whereT = array('name'=>'tentang');
			$update['tentang'] = $this->M_global->update('kel_web', $dataT, $whereT);
			echo "<script>alert('Data berhasil diperbarui.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		if($this->input->post('chFasilitas')){
			$dataF = array('value'=>$this->input->post('fasilitas'));
			$whereF = array('name'=>'fasilitas');
			$update['fasilitas'] = $this->M_global->update('kel_web', $dataF, $whereF);
			echo "<script>alert('Data berhasil diperbarui.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		if($this->input->post('chPeraturan')){
			$dataP = array('value'=>$this->input->post('peraturan'));
			$whereP = array('name'=>'peraturan');
			$update['peraturan'] = $this->M_global->update('kel_web', $dataP, $whereP);
			echo "<script>alert('Data berhasil diperbarui.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		$data['peraturan'] = $this->M_global->confGet('peraturan');
		$data['harga'] = $this->M_global->confGet('harga');
		$data['tentang'] = $this->M_global->confGet('tentang');
		$data['fasilitas'] = $this->M_global->confGet('fasilitas');
		$data['alamat'] = $this->M_global->confGet('alamat');
		$data['kontak'] = $this->M_global->confGet('kontak');
		$data['bank'] = $this->M_global->confGet('bank');
		$data['rekening'] = $this->M_global->confGet('rekening');
		$data['nama_rek'] = $this->M_global->confGet('nama_rek');

		$this->load->view('skeleton/skeleton_header');
		$this->load->view('skeleton/skeleton_navbar');
		$this->load->view('admin/v_config',$data);
		$this->load->view('skeleton/skeleton_footer');
	}

	public function laporan($qry=''){
		if($this->session->userdata('LEVEL') != '99'){
			redirect('admin/panel');
		}
		
		
		if($qry==''){
			$data['laporan'] = $this->M_global->custom("SELECT DATE_FORMAT(stamp,'%Y-%m') AS `date`, SUM(harga) AS kotor, SUM(dp) AS bersih, (SUM(harga)-SUM(dp)) AS selisih FROM `invoice` GROUP BY `date` ORDER BY `date` DESC");
			$data['harian'] = false;
		}else{
			$data['laporan'] = $this->M_global->custom("SELECT DATE_FORMAT(stamp,'%Y-%m-%d') AS `date`, SUM(harga) AS kotor, SUM(dp) AS bersih, (SUM(harga)-SUM(dp)) AS selisih FROM `invoice` WHERE DATE_FORMAT(stamp,'%Y-%m') = '$qry' GROUP BY `date` ORDER BY `date` DESC");
			$data['harian'] = true;
		}

		$this->load->view('skeleton/skeleton_header');
		$this->load->view('skeleton/skeleton_navbar');
		$this->load->view('admin/v_laporan',$data);
		$this->load->view('skeleton/skeleton_footer');
	}
	
}
