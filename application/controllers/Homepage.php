<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

	function __construct(){
        parent::__construct();
		$this->load->model('M_global');
	}
	public function index(){

		$data['tentang'] = $this->M_global->confGet('tentang');
		$data['fasilitas'] = $this->M_global->confGet('fasilitas');
		$data['kontak'] = $this->M_global->confGet('kontak');
		$data['alamat'] = $this->M_global->confGet('alamat');

		$data['event'] = $this->M_global->custom("SELECT * FROM `event` ORDER BY tanggal DESC LIMIT 3");

		$this->load->view('skeleton_homepage/skeleton_header');
		$this->load->view('skeleton_homepage/skeleton_navbar');
		$this->load->view('homepage/v_home',$data);
		$this->load->view('skeleton_homepage/skeleton_footer');
	}
	public function event($id=''){

		$this->load->view('skeleton_homepage/skeleton_header');
		$this->load->view('skeleton_homepage/skeleton_navbar');
		
		if($id==''){
			if ($this->input->get('page')){
				$page = $this->input->get('page');
			} else {
				$page = 1;
			}
			$show = 3;
			$offset = ($page-1) * $show;

			$total_rows = $this->M_global->countAll('COUNT(*) as jml','event');
			$total = ceil($total_rows->jml / $show);

			$data['event'] = $this->M_global->custom("SELECT * FROM `event` ORDER BY tanggal DESC LIMIT $offset, $show");
			$data['total_pages'] = $total;
			$this->load->view('homepage/v_event',$data);
		}else{
			$id_event = $id;
			$where=array('id_event' => $id_event);
			$data['event'] = $this->M_global->getOne('event',$where);
			$this->load->view('homepage/v_singleevent',$data);
		}
		
		$this->load->view('skeleton_homepage/skeleton_footer');
	}
	
}
