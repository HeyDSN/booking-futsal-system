<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('M_global');
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function index(){
        redirect('user/login');
    }

    public function getWkt(){
        $return =  "<option value='' disabled selected>-- Pilih Durasi -- </option>";

        $lap = $this->input->post('lap');
        $tgl = $this->input->post('tgl');
        $awal = $this->input->post('jam');

        $start = strtotime($awal);
        $startx = strtotime($awal);

        $wkt = 0;
        $err = 0;

        $where = array(
            'lapangan' => $lap,
            'book_date' => $tgl,
            'status' => 1
        );

        
        $results = $this->M_global->get('book',$where,'book_time');
        $output = array();
        foreach($results as $result){
            $array = json_decode($result->book_time,true);
            $output = array_merge($output, $array);
        }

        array_push($output,"22:00");

        while ($wkt < 5){
            $wkt++;

            $startx = strtotime('+30 minutes',$startx);
            $rangex = date('H:i', $startx);

            if (in_array($rangex, $output)) {
                break;
            }

            $start = strtotime('+60 minutes',$start);
            $range = date('H:i', $start);

            $end = strtotime('22:00:00');
            $max = ($end - $start)/3600; 

            if (in_array($range, $output)) {
                $err++;
            }

            if($wkt == 1){
                $return .= "<option value='$wkt'>$wkt Jam</option>"; 
            }else if($err == 2 or $hasil >= $max){
                break;
            }else{
                $ceking = strtotime('-30 minutes',$start);
                $range = date('H:i', $ceking);

                if (in_array($range, $output)) {
                    break;
                }
                $return .= "<option value='$wkt'>$wkt Jam</option>"; 
            }
        }
        echo $return;
    }

    public function getJam(){
        $return = "<option value='' disabled selected>-- Pilih Jam -- </option>";

        $lap = $this->input->post('lap');
        $tgl = $this->input->post('tgl');

        $buka='07:00';
        $tutup='21:00';

        $start = strtotime($buka);
        $end = strtotime($tutup);

        $where = array(
            'lapangan' => $lap,
            'book_date' => $tgl,
            'status' => 1
        );
        
        $results = $this->M_global->get('book',$where,'book_time');
        $output = array();
        foreach($results as $result){
            $array = json_decode($result->book_time,true);
            $output = array_merge($output, $array);
        }

        while ($start !== $end){

            $start = strtotime('+30 minutes',$start);
            $range = date('H:i', $start);

            if (in_array($range, $output)) {
                $return .=  "<option value='$range' style='color: red' disabled>$range</option>";
            }else{
                $return .=  "<option value='$range' style='color: green'>$range</option>"; 
            }

        }

        echo $return;
    }

    public function getJamHome(){
        $return = "<tr><th>No</th><th>Jam Main</th><th>Status</th></tr>";

        $lap = $this->input->post('lap');
        $tgl = $this->input->post('tgl');

        $buka='07:00';
        $tutup='21:00';

        $start = strtotime($buka);
        $end = strtotime($tutup);

        $where = array(
            'lapangan' => $lap,
            'book_date' => $tgl,
            'status' => 1
        );
        
        $results = $this->M_global->get('book',$where,'book_time');
        $output = array();
        foreach($results as $result){
            $array = json_decode($result->book_time,true);
            $output = array_merge($output, $array);
        }
        $no = 0;
        while ($start !== $end){
            $no++;
            $start = strtotime('+30 minutes',$start);
            $range = date('H:i', $start);

            if (in_array($range, $output)) {
                $return .=  "<tr><td>$no</td><td>$range</td><td style='color: red'>Tidak Tersedia</td></tr>";
            }else{
                $return .=  "<tr><td>$no</td><td>$range</td><td style='color: green'>Tersedia</td></tr>";
            }

        }

        echo $return;
    }
}