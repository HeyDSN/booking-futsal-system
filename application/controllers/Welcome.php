<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct(){
        parent::__construct();
		redirect('homepage');
	}
	public function index(){
		redirect('homepage');
	}
	
}
